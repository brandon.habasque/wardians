import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { BtnWardiansComponent } from './btn-wardians.component';
@NgModule({
  declarations: [BtnWardiansComponent],
  imports: [FormsModule, IonicModule, CommonModule],
  exports: [BtnWardiansComponent],
})
export class BtnWardiansModule {}
