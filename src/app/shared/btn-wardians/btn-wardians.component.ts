import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-btn-wardians',
  templateUrl: './btn-wardians.component.html',
  styleUrls: ['./btn-wardians.component.scss'],
})
export class BtnWardiansComponent implements OnInit {
  @Input() libelle: string = '';
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  click() {
    this.onClick.emit();
  }
}
