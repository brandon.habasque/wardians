import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./pages/home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'setup',
        loadChildren: () =>
          import('./pages/setup/setup.module').then((m) => m.SetupPageModule),
      },
      {
        path: 'action',
        loadChildren: () =>
          import('./pages/action/action.module').then(
            (m) => m.ActionPageModule
          ),
      },
      {
        path: 'result',
        loadChildren: () =>
          import('./pages/result/result.module').then(
            (m) => m.ResultPageModule
          ),
      },
    ],
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
